const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));


const radiobaseRoutes = require('./routes/radiobase.routes');

app.use(cors());
app.use('/api/', radiobaseRoutes);

app.get('/', (req, res)=>{
    res.send("<h1>Bienvenido</h1>");
});

const server = app.listen(process.env.PORT||5000,()=>{
    const puerto = server.address().port
    console.log(`Servidor ejecutandose correctamente en el puerto ${puerto}...`)
})