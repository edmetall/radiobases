const dbConnection = require('../../config/db.config');

const Radiobase = radiobase => {
    this.radiobase = radiobase.radiobase;
    this.fecha = radiobase.fecha;
    this.region = radiobase.region;
    this.trafico = radiobase.trafico;
};

Radiobase.findAll = result => {
    const query = "SELECT * FROM radiobase ";
    dbConnection.query(query, (err,res)=>{
        if (err){
            console.log("error: ", err);
            result(null,err);
        }else{
            result(null,res);
        }
    });
}

Radiobase.findRadiobase = (radiobase,result) => {
    const query = `SELECT * FROM radiobase WHERE radiobase LIKE '${radiobase.replace(/["']/g, "")}'`;
    dbConnection.query(query,(err,res)=>{
        if (err){
            console.log("error: ", err);
            result(null,err);
        }else{
            result(null,res);
        }
    });
}

Radiobase.findRegion = (region,result) => {
    const query = `SELECT * FROM radiobase WHERE region = ${region}`;
    dbConnection.query(query,(err,res)=>{
        if (err){
            console.log("error: ", err);
            result(null,err);
        }else{
            result(null,res);
        }
    });
}

Radiobase.getRadiobases = (radiobase,result) => {
    const query = `SELECT radiobase FROM radiobase WHERE radiobase LIKE '${radiobase.replace(/["']/g, "")}%' GROUP BY radiobase ORDER BY radiobase`;
    dbConnection.query(query, (err,res)=>{
        if (err){
            console.log("error: ", err);
            result(null,err);
        }else{
            result(null,res);
        }
    });
}

module.exports = Radiobase;