const Radiobase = require('../models/radiobase.model');
exports.findAll = (req,res)=>{
    Radiobase.findAll((err, radiobase)=>{
        console.log('controller')
        if(err)
        res.send(err);
        res.send(radiobase);
    });
}

exports.findRadiobase = (req,res) => {
    Radiobase.findRadiobase(req.params.radiobase,(err, radiobase)=>{
        if(err)
        res.send(err);
        res.json(radiobase)
    });
}

exports.findRegion = (req,res) => {
    Radiobase.findRegion(req.params.region,(err, radiobase)=>{
        if(err)
        res.send(err);
        res.json(radiobase)
    });
}

exports.getRadiobases = (req,res)=>{
    Radiobase.getRadiobases(req.params.radiobase,(err, radiobase)=>{
        if(err)
        res.send(err);
        res.send(radiobase);
    });
}