const express = require('express');
const router = express.Router();

const radiobaseController = require('../controllers/radiobase.controller');

router.get('/', radiobaseController.findAll);

router.get('/bases/:radiobase', radiobaseController.getRadiobases);

router.get('/radiobase/:radiobase', radiobaseController.findRadiobase);

router.get('/region/:region', radiobaseController.findRegion);

module.exports = router;